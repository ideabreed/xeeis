var express = require('express');
var app = express();
var path = require('path');
app.use(express.static(path.join(__dirname+'/public')));
app.set('view engine', 'ejs');
app.set('views', __dirname+'/views');
var page = require('./documents/data.json');
var nodemailer = require('nodemailer');
var page2 = require('./documents/study-abroad.json');
var studyAbroad = page2.country;
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());
// console.log(studyAbroad[0]);

//getting routes
app.get('/:pid', function(req, res){
    // console.log(studyAbroad[0]);
    if(req.params.pid=='study-in-australia'){
        var pageData = studyAbroad[0];
    }
    if(req.params.pid=='study-in-japan'){
         var pageData = studyAbroad[1];
    }
    if(req.params.pid=='study-in-usa'){
         var pageData = studyAbroad[2];
    }
    if(req.params.pid=='study-in-europe'){
         var pageData = studyAbroad[3];
    }
    if(req.params.pid=='study-in-newzealand'){
        var pageData = studyAbroad[4];
    }
    if(req.params.pid=='study-in-canada'){
        var pageData = studyAbroad[5];
    }
    // var studyAbroad = studyAbroad;
    var pages = page;
    // console.log(pageData);
  res.render('home',{
      title: req.params.pid,
      data: pages,
      studyAbroad:pageData
  });
});
//redirectiong page
app.get('/', function(req, res){
    var pages = page;
    res.render('home',{
       title: 'home',
      data: pages,
      studyAbroad: 'hero'
    });
});
app.post('/send', function(req, res){
    console.log(req.body);
    
    let transporter = nodemailer.createTransport({
        service: 'gmail',
        secure: false,
        port: 25,
        auth:{
            user: 'kamal.shahi9988',
            pass:'lamakshahi'
        },
        tls:{
            rejectUnauthorized: true
        }
    });
    var name  = '<P>Sender Description <br> Name: '+req.body.name+'</P>';
    var contact  = '<P> Contact Number: '+req.body.contact+'</P>';
    var interested  = '<P> Interested In : '+req.body.studyAbroad+'</P>';
    var message = '<P> Message: '+req.body.message+'</P> ';
    let HelperOptions = {
        
        from: req.body.email,
        to: 'kamal.shahi9988@gmail.com',
        subject:'New Message From '+ req.body.name+'('+req.body.email+')',
        html: name + contact +interested + message 
    };
    transporter.sendMail(HelperOptions, function(err, info){
        if(err){
            res.send('Sorry Email Not Valid..');
        }else{
            var message = 'Message sent';
            res.send(message);
        }
    });

});
// setting port of app
app.listen(3000, function(){
    console.log('listening to port 3000');
});